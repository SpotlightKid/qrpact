---
author: olegkapitonov
category: ''
date: 2020-07-10 14:25:00 UTC+03:00
description: ''
homepage: https://kpp-tubeamp.com/
link: https://github.com/olegkapitonov/KPP-VST3/releases/tag/1.2.1
repository: https://github.com/olegkapitonov/KPP-VST3.git
slug: kapitonov-plugins-pack-vst3-121
tags: ''
title: Kapitonov Plugins Pack VST3 1.2.1
type: text
---
A set of plugins for guitar sound processing.

Now with also Linux VST3 and experimental Windows VST3 versions.
