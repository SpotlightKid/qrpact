---
author: Open Music Kontrollers
category: ''
date: 2020-04-13 12:00:00 UTC+02:00
description: ''
homepage: https://open-music-kontrollers.ch/lv2/shells_bells/
link: https://gitlab.com/OpenMusicKontrollers/shells_bells.lv2/blob/master/ChangeLog
repository: https://git.open-music-kontrollers.ch/lv2/shells_bells.lv2
slug: shells_bellslv2-040
tags: ''
title: shells_bells.lv2 0.4.0
type: text
---
A just-for-fun LV2 plugin bundle. Sound the bell in the shell to send a MIDI note.

* Now builds with pugl master
* Use spinners instead of dials
* Adapt color theme autmatically  to terminal colors