---
author: Hermann Meyer
category: ''
date: 2020-07-17 07:35:00 UTC+01:00
description: ''
homepage: https://sourceforge.net/projects/guitarix/
link: https://github.com/brummer10/guitarix/blob/master/trunk/changelog
repository: https://sourceforge.net/p/guitarix/git/ci/master/tree/
slug: guitarix2-0410
tags: ''
title: Guitarix2 0.41.0
type: text
---
A virtual guitar amplifier for Linux running with JACK.

* Added NSM support
* Added Slovak translation by Jozef Riha
* Added MIDI out and low/high cut filter to tuner
* Disabled GxVibe, because it is broken
* ... and much more