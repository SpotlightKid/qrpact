---
author: Nils Hilbricht
category: ''
date: 2020-04-15 01:37:00 UTC+02:00
description: ''
homepage: https://www.laborejo.org/vico/
link: https://git.laborejo.org/lss/Vico/src/branch/master/CHANGELOG
repository: http://git.laborejo.org/lss/vico.git
slug: vico-11
tags: ''
title: Vico 1.1
type: text
---
*Vico* is a minimalistic MIDI sequencer that is intended to be used in parallel
with other software.

* Save recording state and use as NSM label
* Fix crash when pitch shifting without selection
* Fixed NSM Save Status
* Better desktop program icons
* Provide unix manpages
* Bug fixes, documentation update
