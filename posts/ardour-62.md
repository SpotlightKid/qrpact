---
author: Paul Davis et al.
category: ''
date: 2020-07-08 01:27:00 UTC+02:00
description: ''
homepage: https://ardour.org
link: https://ardour.org/whatsnew.html
repository: https://github.com/Ardour/ardour
slug: ardour-62
tags: ''
title: Ardour 6.2
type: text
---
A full-featured, cross-platform digital audio workstation (DAW).

See [this link](https://ardour.org/whatsnew.html) for changes.

Please note that there was no 6.1 release.