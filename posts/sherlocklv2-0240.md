---
author: Open Music Kontrollers
category: ''
date: 2020-07-15 23:36:00 UTC+02:00
description: ''
homepage: https://open-music-kontrollers.ch/lv2/sherlock/
link: https://git.open-music-kontrollers.ch/lv2/sherlock.lv2/tree/ChangeLog
repository: https://git.open-music-kontrollers.ch/lv2/sherlock.lv2/tree/ChangeLog
slug: sherlocklv2-0240
tags: ''
title: sherlock.lv2 0.24.0
type: text
---
An LV2 plugin bundle for visualizing LV2 atom, MIDI and OSC events.

* Added support for GL double buffering
