---
author: Damien Zammit
category: ''
date: 2020-07-19 03:32:00 UTC+02:00
description: ''
homepage: http://www.zamaudio.com/?p=976
link: https://github.com/zamaudio/zam-plugins/blob/master/changelog
repository: https://github.com/zamaudio/zam-plugins.git
slug: zam-plugins-313
tags: ''
title: zam-plugins 3.13
type: text
---
A collection of LADSPA/LV2/VST/JACK audio plugins for high-quality processing.

Small bug and compatibility fixes.

