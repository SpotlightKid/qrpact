---
author: Open Music Kontrollers
category: ''
date: 2020-04-13 12:00:00 UTC+02:00
description: ''
homepage: https://open-music-kontrollers.ch/lv2/sherlock/
link: https://git.open-music-kontrollers.ch/lv2/sherlock.lv2/tree/ChangeLog
repository: https://git.open-music-kontrollers.ch/lv2/sherlock.lv2/tree/ChangeLog
slug: sherlocklv2-0220
tags: ''
title: sherlock.lv2 0.22.0
type: text
---
An LV2 plugin bundle for visualizing LV2 atom, MIDI and OSC events.

* Report of decimal note/controller numbers in MIDI inspector
* Fixed scrolling in atom inspector TTL preview
* Now builds with pugl master
