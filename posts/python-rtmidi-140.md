---
author: Christopher Arndt
category: ''
date: 2020-01-19 17:46:23 UTC+01:00
description: ''
homepage: http://trac.chrisarndt.de/code/wiki/python-rtmidi
link: https://github.com/SpotlightKid/python-rtmidi/blob/master/CHANGELOG.rst
repository: https://github.com/SpotlightKid/python-rtmidi.git
slug: python-rtmidi-140
tags: ''
title: python-rtmidi 1.4.0
type: text
---
Python bindings for the cross-platform MIDI I/O library RtMidi

* Dropped nominal Python 2 support.
* Examples and documentation improvements.
