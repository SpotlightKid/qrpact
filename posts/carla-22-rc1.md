---
author: Filipe Coelho
category: ''
date: 2020-07-18 21:29:00 UTC+02:00
description: ''
homepage: https://kx.studio/Applications:Carla
link: https://kx.studio/News/?action=view&url=carla-22-rc1-is-here
repository: https://github.com/falkTX/Carla.git
slug: carla-22-rc1
tags: ''
title: Carla 2.2-RC1
type: text
---
A fully-featured modular audio plugin host.

* Linux VST3 support
* Better multi-client & instance handling
* Small UI/UX improvements all around