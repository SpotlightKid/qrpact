---
author: Nils Hilbricht
category: ''
date: 2020-07-14 20:23:00 UTC+02:00
description: ''
homepage: https://www.laborejo.org/vico/
link: https://git.laborejo.org/lss/Vico/src/branch/master/CHANGELOG
repository: http://git.laborejo.org/lss/vico.git
slug: vico-12
tags: ''
title: Vico 1.2
type: text
---
*Vico* is a minimalistic MIDI sequencer that is intended to be used in parallel
with other software.

* Allow recording right from program start
* Fix crashes and problems with recent Qt 5.15 release
* Quit under NSM works now
* Fix accidental transparency in Wayland
* Bug fixes, documentation update.