---
author: Christopher Arndt
category: ''
date: 2020-07-18 17:35:00 UTC+02:00
description: ''
homepage: https://spotlightkid.github.io/python-rtmidi/
link: https://github.com/SpotlightKid/python-rtmidi/blob/master/CHANGELOG.rst
repository: https://github.com/SpotlightKid/python-rtmidi.git
slug: python-rtmidi-142
tags: ''
title: python-rtmidi 1.4.2
type: text
---
Python bindings for the cross-platform MIDI I/O library RtMidi.

* Added more helpful aliases for MIDI events/controllers constants
* Added `ccstore` example to print last seen controller change values