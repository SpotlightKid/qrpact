---
author: Open Music Kontrollers
category: ''
date: 2020-04-13 12:00:00 UTC+02:00
description: ''
homepage: https://open-music-kontrollers.ch/lv2/moony/
link: https://git.open-music-kontrollers.ch/lv2/moony.lv2/tree/ChangeLog
repository: https://git.open-music-kontrollers.ch/lv2/moony.lv2
slug: moonylv2-0320
tags: ''
title: moony.lv2 0.32.0
type: text
---
Write LV2 control port and event filters in Lua.

* Added next UI (not feature-complete, yet replaces simple UI)
* Deprecated simple UI
* Now builds with pugl master