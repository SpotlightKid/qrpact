---
author: houston4444
category: ''
date: 2020-07-16 16:34:51 UTC+02:00
description: ''
homepage: https://github.com/Houston4444/RaySession
link: https://github.com/Houston4444/RaySession/blob/master/CHANGELOG
repository: https://github.com/Houston4444/RaySession.git
slug: raysession-090
tags: ''
title: RaySession 0.9.0
type: text
---
A GNU/Linux NSM-compatible session manager for audio programs.

* CLI tool to control GUI actions
* Session load, save, close shell scripts
* JACK configuration session script
* New RayHack client protocol
* ... and much more
