---
author: Open Music Kontrollers
category: ''
date: 2020-07-15 23:36:00 UTC+02:00
description: ''
homepage: https://open-music-kontrollers.ch/lv2/mephisto/
link: https://git.open-music-kontrollers.ch/lv2/mephisto.lv2/tree/ChangeLog
repository: https://git.open-music-kontrollers.ch/lv2/mephisto.lv2
slug: mephistolv2-0120
tags: ''
title: mephisto.lv2 0.12.0
type: text
---
Write LV2 audio/CV instruments/filters directly in your host using the FAUST
DSP language.

* Added support for bargraphs
* Added preset for simple VU meter
* Added momentary button ui-widget
* Added support for `ui:scaleFactor` option
* Added support for GL double buffering
* Changed all ui-widgets to show labels and correct value ranges
