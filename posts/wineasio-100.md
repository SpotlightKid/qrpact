---
author: Filipe Coelho
category: ''
date: 2020-07-14 04:42:00 UTC+02:00
description: ''
homepage: https://github.com/wineasio/wineasio
link: https://github.com/wineasio/wineasio/releases/tag/v1.0.0
repository: https://github.com/wineasio/wineasio.git
slug: wineasio-100
tags: ''
title: WineASIO 1.0.0
type: text
---
WineASIO provides an ASIO to JACK driver for WINE.

* Maintainer changed from Joakim Hernberg to Filipe Coelho (falkTX).
* Added custom PyQt5 GUI for WineASIO settings
* Fixed control panel startup
* Fixed code to work with latest Wine
* Build and packaging fixes & improvements

