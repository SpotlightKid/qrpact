---
author: olegkapitonov
category: ''
date: 2020-01-11 12:00:00 UTC+01:00
description: ''
homepage: https://github.com/olegkapitonov/Kapitonov-Plugins-Pack
link: https://github.com/olegkapitonov/Kapitonov-Plugins-Pack/releases
repository: https://github.com/olegkapitonov/Kapitonov-Plugins-Pack
slug: kapitonov-plugins-pack-11
tags: ''
title: Kapitonov Plugins Pack 1.1
type: text
---
A set of plugins for guitar sound processing.

Bugfix release.
