---
author: Hermann Meyer
category: ''
date: 2020-07-14 09:51:00 UTC+02:00
description: ''
homepage: https://github.com/brummer10/FatFrog.lv2
link: https://github.com/brummer10/FatFrog.lv2/releases/tag/v1.0
repository: https://github.com/brummer10/FatFrog.lv2.git
slug: fatfroglv2-10
tags: ''
title: FatFrog.lv2 1.0
type: text
---
A valve amplifier simulation LV2 plugin to let the walls rattle.

Initial release.