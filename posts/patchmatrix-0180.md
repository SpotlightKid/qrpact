---
author: Open Music Kontrollers
category: ''
date: 2020-04-13 12:00:00 UTC+02:00
description: ''
homepage: https://open-music-kontrollers.ch/lad/patchmatrix/
link: https://git.open-music-kontrollers.ch/lad/patchmatrix/tree/ChangeLog
repository: https://git.open-music-kontrollers.ch/lad/patchmatrix
slug: patchmatrix-0180
tags: ''
title: patchmatrix 0.18.0
type: text
---
A graphical JACK patchbay combining a matrix-style and flow canvas interface.

* Now builds with pugl master
