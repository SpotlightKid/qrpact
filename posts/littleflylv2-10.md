---
author: Hermann Meyer
category: ''
date: 2020-07-14 09:51:00 UTC+02:00
description: ''
homepage: https://github.com/brummer10/LittleFly.lv2
link: https://github.com/brummer10/LittleFly.lv2/releases/tag/v1.0
repository: https://github.com/brummer10/LittleFly.lv2.git
slug: littleflylv2-10
tags: ''
title: LittleFly.lv2 1.0
type: text
---
An overdrive/distortion pedal simulation LV2 plugin.

The perfect pre-processor for **FatFrog**.

Initial release.
