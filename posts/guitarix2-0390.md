---
author: Hermann Meyer
category: ''
date: 2020-01-14 09:41:00 UTC+01:00
description: ''
homepage: https://sourceforge.net/projects/guitarix/
link: ''
repository: https://sourceforge.net/p/guitarix/git/ci/master/tree/
slug: guitarix2-0390
tags: ''
title: Guitarix2 0.39.0
type: text
---
A virtual guitar amplifier for Linux running with jack (Jack Audio
Connection Kit).

This is a maintenance release which mainly is meant to get rid of the
python2 build dependency.
<!-- TEASER_END -->
Changes are so far:

- updated waf version to 2.0.19 by Andreas Degert

- cleanup all GTK(mm) calls to be able to build with `-DGSEAL_ENABLE` flag  (prepare to update to GTK3) by Hubert Figuière

- update Russian Translation by Valeriy Shtobbe and Olesya Gerasimenko (Basealt Translation Team)

- bind `lv2:enabled` to guitarix on_off switch and remove from UI by Hermann Meyer

- update all build scripts to use faust version 2.15.11 by Hermann Meyer

- add new option `-E --hideonquit`, to make the UI experience smooth
when used as LV2 plugin (via Carla export) by Hermann Meyer

- add exit handler and warning when sample-rate is above 96kHz by Hermann Meyer

You can get it here:

<https://sourceforge.net/projects/guitarix/>