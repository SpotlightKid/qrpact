---
author: Glen MacArthur
category: ''
date: 2020-01-15 07:50:24 UTC+01:00
description: ''
homepage: http://www.bandshed.net/avlinux/
link: https://linuxmusicians.com/viewtopic.php?f=24&t=20907
repository: http://download.linuxaudio.org/avlinux/
slug: av-linux-2020114-rc
tags: ''
title: AV Linux 2020.1.14 RC
type: text
---
AV Linux is a Linux-based operating system aimed for multimedia content creators.

This release candidate is more about making sure what is there is all working than adding programs and new
features at this point. Time and feedback are appreciated!
