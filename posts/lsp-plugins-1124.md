---
author: Vladimir Sadovnikov
category: ''
date: 2020-07-16 20:40:00 UTC+02:00
description: ''
homepage: https://lsp-plug.in/
link: https://github.com/sadko4u/lsp-plugins/blob/master/CHANGELOG.txt
repository: https://github.com/sadko4u/lsp-plugins.git
slug: lsp-plugins-1124
tags: ''
title: LSP Plugins 1.1.24
type: text
---
A collection of multi-format open-source plugins.

* New Loudness Compensator and Surge Filter plugin series
* Significant changes to Limiter series
* Better LV2 state support
* Support of loading Hydrogen drumkits by Multisampler
* ... and lots more