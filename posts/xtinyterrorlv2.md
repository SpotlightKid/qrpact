---
author: Hermann Meyer
category: ''
date: 2020-04-15 13:43:00 UTC+02:00
description: ''
homepage: ''
link: https://linuxmusicians.com/viewtopic.php?f=44&t=21321&p=117359#p117359
repository: https://github.com/brummer10/XTinyTerror.lv2
slug: xtinyterrorlv2
tags: ''
title: XTinyTerror.lv2
type: text
draft: true
---
Valve amplifier simulation modelled after the Orange Tiny Terror.

No versioned release yet.
