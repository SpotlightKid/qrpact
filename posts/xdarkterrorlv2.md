---
author: Hermann Meyer
category: ''
date: 2020-04-19 03:34:00 UTC+02:00
description: ''
homepage: ''
link: https://linuxmusicians.com/viewtopic.php?p=117593#p117593
repository: https://github.com/brummer10/XDarkTerror.lv2
slug: xdarkterrorlv2
tags: ''
title: XDarkTerror.lv2
type: text
draft: true
---
Valve amplifier simulation modelled after the Orange Dark Terror.

No versioned release yet.
