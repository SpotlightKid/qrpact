---
author: Alexandros Theodotou
category: ''
date: 2020-07-18 02:58:00 UTC+02:00
description: ''
homepage: https://www.zrythm.org
link: https://git.zrythm.org/cgit/zrythm/tree/CHANGELOG.md
repository: https://git.zrythm.org/cgit/
slug: zrythm-08694
tags: ''
title: Zrythm 0.8.694
type: text
---
Zrythm is a digital audio workstation designed to be featureful and
easy to use.

Tons of additions, changes and fixes. See the
[changelog](https://git.zrythm.org/cgit/zrythm/tree/CHANGELOG.md) for more
information.
