---
author: Filipe Coelho
category: ''
date: 2020-01-16 04:40:00 UTC+01:00
description: ''
homepage: https://kx.studio/Applications:Carla
link: https://kx.studio/News/?action=view&url=carla-21-rc1-is-here
repository: https://github.com/falkTX/Carla.git
slug: carla-21-rc1
tags: ''
title: Carla 2.1-RC1
type: text
---
A fully-featured modular audio plugin host

Better CV support, High-DPI support, proper theme and Carla-Control on Windows,
VST2 plugin for macOS and Windows, Wine-native bridge, improved add-plugin dialog
and favorite plugins, single-page and grouped plugin parameters... ... and a lot more.
